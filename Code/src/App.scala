import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.awt.Graphics
import java.io._
import scala.math._

import java.awt.image.DataBufferByte
import java.awt.image.DataBufferInt
import java.nio.ByteBuffer

object App {

	var bimg: BufferedImage = null;

	def main(args: Array[String]): Unit = {

		val imgName: String = "marilyn"
		val ext: String = "png"

		// Load image
		val img: File = new File("img/" + imgName + "." + ext)
		val bimg: BufferedImage = ImageIO.read(img)
		this.bimg = bimg

		// Transforming process
		/*for(y <- 0 until bimg.getHeight) {
			for(x <- 0 until bimg.getWidth) {
				var intPixel: Int = bimg getRGB (x, y)
				var rgbPixel: (Int, Int, Int) = toRGB(intPixel)
				var transformed: (Int, Int, Int) = grayscale(rgbPixel)
				bimg setRGB (x, y, toIntRGB(transformed))
			}
		}*/

		// TEST

		val fft : FFT = new FFT()
		// val matrix: Array[Complex] = imageToArray(bimg)
		// val matrixFFT: Array[Complex] = fft.fftShift(fft.fft(matrix), bimg.getWidth)
		// val bimgFFT = arrayToImage(matrixFFT, bimg.getWidth, bimg.getHeight)
		val filtered: BufferedImage = filterDFT(bimg, makeGaussianFilter(bimg.getWidth, bimg.getHeight, 20))

		// Write image
		val outputImg: File = new File("img/" + imgName + "-out." + ext)
		ImageIO.write(filtered, ext, outputImg)
	}

	/**
	 * Convert an integer pixel in the default RGB color model (TYPE_INT_ARGB) to a tuple (Red, Green, Blue)
	 */
	def toRGB(x: Int): (Int, Int, Int) = ((x >> 16) & 0xFF, (x >> 8) & 0xFF, x & 0xFF)

	/*
	 * Convert an integer pixel in the default RGB color model (TYPE_INT_ARGB) to a tuple (Alpha, Red, Green, Blue)
	 */
	def toARGB(x: Int): (Int, Int, Int, Int) = ((x >> 24) & 0xFF, (x >> 16) & 0xFF, (x >> 8) & 0xFF, x & 0xFF)

	/**
	 * Convert a tuple (Red, Green, Blue) to an integer pixel
	 */
	def toIntRGB(rbg: (Int, Int, Int)): Int = rbg._1 * 65536 + rbg._2 * 256 + rbg._3

	/**
	 * Converti une couleur RGB en niveau de gris
	 *
	 * @param rgb 	valeur du pixel décomposée en RGB
	 * @return 		un tuple des trois composantes RGB modifiées
	 */
	def grayscale(rgb: (Int, Int, Int)): (Int, Int, Int)= {
		val average = (rgb._1 + rgb._2 + rgb._3) / 3
		return (average, average, average)
	}

	/**
	 * Converti une couleur RGB en négatif
	 *
	 * @param rgb 	valeur du pixel décomposée en RGB
	 * @return 		un tuple des trois composantes RGB modifiées
	 */
	def negative(rgb: (Int, Int, Int)): (Int, Int, Int)= {
		return (255-rgb._1, 255-rgb._2, 255-rgb._3)
	}

	/**
	 * Change la saturation d'un pixel
	 *
	 * @param rgb 	valeur du pixel décomposée en RGB
	 * @param s 	la niveau de saturation, le taux varie entre 0.0 (complètement désaturé) à 1.0 (inchangé)
	 * @return 		un tuple des trois composantes RGB modifiées
	 */
	def saturate(rgb: (Int, Int, Int), s: Double): (Int, Int, Int)= {
		val r = (0.213 + 0.787 * s) * rgb._1.toDouble + 0.715 * (1.0 - s) * rgb._2.toDouble + 0.072 * (1.0 - s) * rgb._3.toDouble
		val g = 0.213 * (1.0 - s) * rgb._1.toDouble + (0.715 + 0.285 * s) * rgb._2.toDouble + 0.072 * (1.0 - s) * rgb._3.toDouble
		val b = 0.213 * (1.0 - s) * rgb._1.toDouble + 0.715 * (1.0 - s) * rgb._2.toDouble + (0.072 + 0.928 * s) * rgb._3.toDouble
		return (r.toInt, g.toInt, b.toInt)
	}

	/**
	 * Fonction permettant de construire un filtre Gaussien
	 *
	 * @param width 				largeur de l'image filtre
	 * @param heigth 				hauteur de l'image filtre
	 * @param sigma 				valeur de sigma
	 * @param highPass (optionnel) 	true si il s'agit d'un filtre pour les fréquences hautes, false pour les fréquences basses
	 * @return un tableau de Double contenant les coefficients du filtre de la taille voulue
	 */
	def makeGaussianFilter(width: Int, height: Int, sigma: Int, highPass: Boolean = true): Array[Array[Double]] = {
		val centerX = if ((width%2) == 1) (width/2).toInt + 1 else (width/2)
		val centerY = if ((height%2) == 1) (height/2).toInt + 1 else (height/2)

		def gaussian(x: Int, y: Int): Double = {
			val coefficient = Math.exp(-1.0 * (Math.pow(x - centerX, 2) + Math.pow(y - centerY, 2)) / (2 * Math.pow(sigma, 2)))
			return if (highPass) (1-coefficient) else coefficient
		}

		var coefficients: Array[Array[Double]] = Array.ofDim[Double](width, height)

		for(y <- 0 until height) {
			for(x <- 0 until width) {
				coefficients(x)(y) = gaussian(x,y)
				println(coefficients(x)(y))
			}
		}

		return coefficients;
	}

	/**
	 * Fonction permettant de convertir une BufferedImage en un tableau de nombres complexes
	 *
	 * @param imageMatrix 	l'image à convertir
	 * @return le tableau de complexes obtenu
	 */
	def imageToArray(imageMatrix: BufferedImage): Array[Complex] = {
		var pixels: Array[Complex] = new Array[Complex](imageMatrix.getWidth * imageMatrix.getHeight)

		for(y <- 0 until imageMatrix.getHeight) {
			for(x <- 0 until imageMatrix.getWidth) {
				val pixel: Int = toRGB(imageMatrix.getRGB(x, y))._1
				// val pixel: Int = imageMatrix.getRGB(x, y) + 0xFFFFFF + 1
				// pixels(imageMatrix.getWidth * y + x) = new Complex(Math.cos(pixel), Math.sin(pixel))
				pixels(imageMatrix.getWidth * y + x) = new Complex(pixel, 0.0)
			}
		}

		return pixels
	}

	/**
	 * Fonction permettant de convertir un tableau de nombres complexes en une BufferedImage
	 *
	 * @param data 		le tableau de complexes à convertir
	 * @param width 	la largeur de l'image à générer
	 * @param height 	la hauteur de l'image à générer
	 * @return l'image obtenue
	 */
	def arrayToImage(matrix: Array[Complex], width: Int, height: Int): BufferedImage = {
		for(x <- 0 until matrix.length) {
			val pixel: Int = matrix(x).re.toInt
			this.bimg.setRGB(x % width, x / width, pixel)
		}

		return this.bimg
	}

	def arrayToImage(matrix: Array[Double], width: Int, height: Int): BufferedImage = {
		var imageMatrix: BufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

		for(x <- 0 until matrix.length) {
			val pixel: Int = matrix(x).toInt
			imageMatrix.setRGB(x % width, x / width, 0xFFFFFF - toIntRGB(pixel,pixel,pixel))
		}

		return imageMatrix
	}

	/**
	 * Fonction permettant d'appliquer le filtre gaussien sur une image. L'application se fait sur la transformée de celle-ci par la transformation de Fourrier rapide, puis cette transformée est inversé pour retrouver une image modifiée.
	 *
	 * @param imageMatrix 	l'image concerné
	 * @param filterMatrix 	un tableau des coefficients du filtre Gaussien à appliquer sur l'image
	 * @return l'image modifiée (les fréquences hautes ou basses sont gardées en fonction du filtre appliqué)
	 */
	def filterDFT(imageMatrix: BufferedImage, filterMatrix: Array[Array[Double]]): BufferedImage = {

		val height: Int = imageMatrix.getHeight
		val width: Int = imageMatrix.getWidth
		val height2: Int = filterMatrix.length
		val width2: Int = filterMatrix(0).length

		if (width != height2) throw new Exception("Matrices don't match")

		// Créé un objet de transformation de Fourrier rapide
		val fft : FFT = new FFT()

		// Converti l'image en tableau
		val matrix: Array[Complex] = imageToArray(imageMatrix)
		// On applique la FFT et on centre
		val matrixFFT: Array[Complex] = fft.fftShift(fft.fft(matrix), imageMatrix.getWidth)

		// On applique le filtre (produit matriciel)
		val matrixFiltered: Array[Complex] = new Array[Complex](matrixFFT.length) // height * width2
		/*for(y <- 0 until height) {
			for(x <- 0 until width2) {
				for(k <- 0 until width) {
					val pixelImage: Complex = matrixFFT(width * k + x)
					val pixelFilter: Double = filterMatrix(k)(y) // new Complex(Math.cos(filterMatrix(k)(y)), Math.sin(filterMatrix(k)(y)))
					val pixelFiltered: Complex = pixelImage.times(new Complex(pixelFilter,1.0))

					if (matrixFiltered(width * y + x) == null) matrixFiltered(width * y + x) = new Complex(0.0, 0.0)

					matrixFiltered(width * y + x) = matrixFiltered(width * y + x).plus(pixelFiltered)
					//matrixFiltered(x)(y) += imageMatrix(x)(k) * filterMatrix(k)(y)
				}
			}
		}*/

		/*for(y <- 0 until height) {
			for(x <- 0 until width) {
				val pixelImage: Complex = matrixFFT(width * y + x)
				val pixelFilter: Double = filterMatrix(width * y + x)
				//val pixelFilter: (Int, Int, Int) = toRGB(filterMatrix.getRGB(x, y))

				matrixFiltered(width * y + x) = pixelImage.times(new Complex(pixelFilter, 0.0))
			}
		}*/

		// On "décentre" et applique la FFT inverse
		var matrixIFFT: Array[Complex] = fft.ifft(fft.ifftShift(matrixFiltered, width))

		// On retourne l'image modifiée
		for(x <- 0 until matrixIFFT.length) {
			val pixel: Int = matrixIFFT(x).abs().toInt
			imageMatrix.setRGB(x % width, x / width, toIntRGB(pixel,pixel,pixel))
		}

		return imageMatrix
	}

	/**
	 * Fonction permettant de garder les fréquences basses d'une image
	 *
	 * @param imageMatrix 		l'image concerné
	 * @param sigma 			valeur du sigma pour le filtre Gaussien
	 * @return une nouvelle image à partir des fréquences basses de l'image
	 */
	def lowPass(imageMatrix: BufferedImage, sigma: Int): BufferedImage = {
		return filterDFT(imageMatrix, makeGaussianFilter(imageMatrix.getWidth, imageMatrix.getHeight, sigma, false))
	}

	/**
	 * Fonction permettant de garder les fréquences hautes d'une image
	 *
	 * @param imageMatrix 		l'image concerné
	 * @param sigma 			valeur du sigma pour le filtre Gaussien
	 * @return une nouvelle image à partir des fréquences hautes de l'image
	 */
	def highPass(imageMatrix: BufferedImage, sigma: Int): BufferedImage = {
		return filterDFT(imageMatrix, makeGaussianFilter(imageMatrix.getWidth, imageMatrix.getHeight, sigma, true))
	}

	/**
	 * Fonction permettant de construire une image hybride à partir de deux images (merge)
	 *
	 * @param highFreqImg 	l'image dont les fréquences hautes seront gardées
	 * @param lowFreqImg 	l'image dont les fréquences basses seront gardées
	 * @param sigmaHigh		la valeur du sigma pour le filtre Gaussien pour les fréquences hautes
	 * @param sigmaLow		la valeur du sigma pour le filtre Gaussien pour les fréquences basses
	 * @return une nouvelle image hybride à partir des fréquences hautes et des fréquences basses des images correspondantes
	 */
	def hybridImage(highFreqImg: BufferedImage, lowFreqImg: BufferedImage, sigmaHigh: Int, sigmaLow: Int): BufferedImage = {
		val highPassed = highPass(highFreqImg, sigmaHigh)
		val lowPassed = lowPass(lowFreqImg, sigmaLow)

		val w = Math.max(highPassed.getWidth(), lowPassed.getWidth());
		val h = Math.max(highPassed.getHeight(), lowPassed.getHeight());
		val combined: BufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		val g: Graphics = combined.getGraphics();
		g.drawImage(highPassed, 0, 0, null);
		g.drawImage(lowPassed, 0, 0, null);

		return combined
	}

	/**
	 * Fonction permettant de prendre la valeur des complexes (valeur absolue) du spectre
	 * et permet d'harmoniser ces valeurs entre 0 et 255
	 *
	 * @param data 	le tableau de complexes à traiter
	 * @return un tableau de Double
	 */
    def scaleSpectrum(data: Array[Complex]): Array[Double] = {
        val out: Array[Double] = new Array[Double](data.length)
        for(x <- 0 until data.length) {
            out(x) = math.log10(data(x).abs + 1)
        }
        val max = out.max

        for(x <- 0 until data.length) {
            val offset: Double = 1.65
            val iRGB = max - out(x)
            val toRGB = (((255.0 / offset) * iRGB) / max).toInt
            out(x) = toRGB
        }
        out
    }
}
