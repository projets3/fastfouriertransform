import scala.math._

class FFT() {

	private var addedPixels = 0

	/**
	 * Fonction FFT
	 *
	 * @param signal 	le signal à transformer
	 * @return le signal transformé
	 */
	def fft(signal: Array[Complex]): Array[Complex] = {
		val n: Int = signal.length

		if(n % 2 != 0){
			adjustComplexList2PowerOf2(signal)
			// throw new RuntimeException("n is not a power of 2")
		}


		if (n == 1) {
			return signal
		}else{
			// fft of even terms
			var even: Array[Complex] = new Array[Complex](n/2)
			even = fft(for (i <- (0 until n by 2).toArray) yield signal(i))

			// fft of odd terms
			var odd: Array[Complex] = new Array[Complex](n/2)
			odd = fft(for (i <- (1 until n by 2).toArray) yield signal(i))

			// combine
			var combined: Array[Complex] = new Array[Complex](n)
			for(m <- 0 until n/2) {
				val o: Double = -2.0 * m * Math.PI / n
				val omega: Complex = new Complex(Math.cos(o), Math.sin(o))
				combined(m) = even(m).plus(omega.times(odd(m)))
				combined(m + n/2) = even(m).minus(omega.times(odd(m)))
			}
			return combined
		}


	}

	/**
	 * Fonction FFT inverse
	 *
	 * @param signal 	le signal à transformer
	 * @return le signal transformé
	 */
	def ifft(signal: Array[Complex]): Array[Complex] = {
		val n: Int = signal.length


		// take the conjugate and compute FFT
		var timeSignal: Array[Complex] = new Array[Complex](n)
		timeSignal = fft(for (i <- (0 until n).toArray) yield signal(i).conjugate())

		// return an array of the nth timeSignal conjugate divide by n
		return (for (i <- (0 until timeSignal.length).toArray) yield timeSignal(i).conjugate().divides(new Complex(n, 0.0))) take (n - this.addedPixels)
	}

	/**
	 * Fonction permettant de déplacer le composant de fréquence zéro au centre du spectre.
	 * Le traitement se fait ici sur une matrice a une dimension. La largeur de la matrice qui a été applatie est donc à renseigner.
	 *
	 * @param x 		le spectre concerné
	 * @param width 	la largeur de la matrice initiale
	 * @return le spectre modifié
	 */
	def fftShift(x: Array[Complex], width: Int): Array[Complex] = {

		/**
		 * Effectue le déplacement
		 *
		 * @param width (optionnel)	la largeur de la matrice initiale si celle-ci est en réalité à 2 dimensions
		 *
		 * {{{
		 * >> shift(Array(0, 3, 1, 4, 5, 2, 6, 7, 9), 3)
		 * Array(6, 7, 9, 0, 3, 1, 4, 5, 2)
		 *
		 * >> shift(Array(0, 1, 2, 3))
		 * Array(2, 3, 0, 1)
		 * }}}
		 */
		def shift(x: Array[Complex], width: Int = 0): Array[Complex] = {
			val n: Int = x.length
			val middle: Int = ((n+1) / 2).toInt + (width/2)

			return x.slice(middle , n) ++ x.slice(0, middle)
		}

		val firstLineShift: Array[Complex] = shift(x.slice(0, width))
		val remainingLinesShift: Array[Complex] = x.slice(width, x.length)
		val tmp: Array[Complex] = firstLineShift ++ remainingLinesShift

		return shift(tmp, width)
	}

	/**
	 * L'inverse de fftShift. Permet de retrouver les positions avant le déplacement.
	 */
	def ifftShift(x: Array[Complex], width: Int): Array[Complex] = {

		def shift(x: Array[Complex], width: Int): Array[Complex] = {
			val n: Int = x.length
			val middle: Int = (n - (n+1) / 2).toInt - (width/2)

			return x.slice(middle , n) ++ x.slice(0, middle)
		}

		val tmp: Array[Complex] = shift(x, width)

		return shift(tmp.slice(0, width), 0) ++ tmp.slice(width, x.length)
	}


	/**
	 * Complète un tableau passé en paramètre de telle sorte que sa taille soit égale à 2^n (n entier) avec une liste de 0 (objet Complex initialisé à 0)
	 */
	def adjustComplexList2PowerOf2(data: Array[Complex]): Array[Complex] = {

		// Logarithme décimal
	    def log2(x: Double):Double = log(x) / log(2.0)

	    // Taille de la liste
	    val size: Double = data.size

	    // Le n de 2^n tq n est entier et que size < 2^n
	    val n: Double = ceil(log2(size))

	    // La taille voulue
	    val sizeWanted: Double = pow(2,n)
	    val diffLength: Int = (sizeWanted - size).toInt
	    this.addedPixels = diffLength

	    val zeros: Array[Complex] = (for(i <- 0 until diffLength) yield new Complex(0, 0)).toArray

	    // On complète le tableau par le nombre de 0 nécessaire (diffLength)
	    return data ++ zeros
	}
}
