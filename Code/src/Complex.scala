import scala.math._

class Complex(real: Double, imag: Double) {
	var re: Double = real // the real part
	var im: Double = imag // the imaginary part

	// return a string representation of the invoking Complex object
	override def toString(): String = "(" + re + ", " + im + ")"

	// return modulus
	def abs(): Double = Math.hypot(re, im) // Math.sqrt(re*re + im*im)

	// return phase/angle
	def phase(): Double = Math.atan2(im, re) // between -pi and pi

	// return a new Complex object whose value is (this + b)
	def plus(b: Complex): Complex = {
		val a: Complex = this
		val real: Double = a.re + b.re
		val imag: Double = a.im + b.im
		return new Complex(real, imag)
	}

	// return a new Complex object whose value is (this - b)
	def minus(b: Complex): Complex = {
		val a: Complex = this
		val real: Double = a.re - b.re
		val imag: Double = a.im - b.im
		return new Complex(real, imag)
	}

	// return a new Complex object whose value is (this * b)
	def times(b: Complex): Complex = {
		val a: Complex = this
		val real: Double = a.re * b.re - a.im * b.im
		val imag: Double = a.re * b.im + a.im * b.re
		return new Complex(real, imag)
	}

	// scalar multiplication
	// return a new object whose value is (this * alpha)
	def times(alpha: Double): Complex = {
		return new Complex(alpha * re, alpha * im)
	}

	// return a new Complex object whose value is the conjugate of this
	def conjugate(): Complex = new Complex(re, -im)

	// return a new Complex object whose value is the reciprocal of this
	def reciprocal(): Complex = {
		val scale: Double = re*re + im*im
		return new Complex(re / scale, -im / scale)
	}

	// return a / b
	def divides(b: Complex): Complex = this.times(b.reciprocal())

}