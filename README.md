# Fast Fourier Transform

This project is an implementation of the Fast Fourier Transform in Scala.

### Author

This project was carried out by Matthias CREACH, Julian DIDIER, Clément GARBAY, Mathieu GOURVES and Damien RAYMOND student at Computer Science Department at Nantes Institute of Technology (France) as part of a mathematical modeling project.

### Use

You can use the makefile to launch the application (main method of the App class). 
Warning, the project is not fully functional you should use the methods as you want to test in this main method.

```sh
$ make
```